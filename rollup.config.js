import VuePlugin from 'rollup-plugin-vue'

export default {
  input: 'src/App.vue',
  output: {
    format: 'esm',
    file: 'dist/TaskList.esm.js',
    exports: 'named',
    name: 'VueTaskList',
    moduleName: 'VueTaskList',
  },
  plugins: [
    VuePlugin()
  ]
}